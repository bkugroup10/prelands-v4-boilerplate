import '../scss/index.scss'

// номера секций
const GREETING = 0,
      AGE      = 1,
      TYPE     = 2,
      GENDER   = 3,
      CLARIFIC = 4,
      TELO     = 5,
      CONFID   = 6,
      EMAIL    = 7,
      FINAL    = 8

function onContentLoad() {
  goTo(GREETING)
  listen_clicks()
}

// перейти к i секции
function goTo(i) {
  // включим слайдер-пагинатор
  if (i > GREETING) {
    $('body').addClass('tall')
  }

  if (i > AGE) {
    if (i === FINAL) {
      $('#nav-slider').hide();
    } else {
      $('#nav-slider').show();
    }
  }
  // инвалидируем позицию пагнатора
  const listItems = [...document.querySelectorAll('#nav-slider li')];
  listItems.map((item) => {
    const id = item.getAttribute('data-section');
    if (id == i) {
      item.classList.add('active');
    } else {
      item.classList.remove('active');
    }
  });

  // включаем видимость секции
  const sections = [...document.querySelectorAll('.section')];
  sections.map((item) => {
    const id = item.getAttribute('data-step');
    if (id == i) {
      item.classList.add('show');
    } else {
      item.classList.remove('show');
    }
  });
}

// слушаем клики по ответам из списка на вопросы
function listen_clicks() {
  $(document)
    .on('click', '#startBtn', function() {
      goTo(AGE)
    })
    .on('click', '#age ul li', function () {     // ответ на вопрос о возрасте
      let use = $(this).attr('data-val')
      try { post_age(use) } catch(e) { console.log('ERROR: post_age() not found')}
      goTo(TYPE)
    })
    .on('click', '#type ul li', function () {    // ответ на вопрос про тип отношений
      goTo(GENDER)
    })
    .on('click', '#gender ul li', function () {  // ответ на вопрос о поле (М - 1, F - 2)
      let use = $(this).attr('data-val')
      try { post_gender(use) } catch(e) { console.log('ERROR: post_gender() not found') }
      goTo(CLARIFIC)
    })
    .on('click', '#clari ul li', function() {    // ответ на вопрос-прояснение
      goTo(TELO)
    })
    .on('click', '#telo ul li', function() {     // ответ на вопрос про тело
      goTo(CONFID)
    })
    .on('click', '#confid ul li', function() {   // ответ на вопрос про конфиденциальность
      goTo(EMAIL)
    })
    .on('click', '#email_submit', function() {   // юзер указал email
      if (emailValid()) {
        let use = $('#email').val()
        try { post_email(use) } catch(e) { console.log('ERROR: post_email() not found') }
        goTo(FINAL)
      } else {
        emailError()
      }
    })
    .on('input', '#email', function() {
      emailErrorClear()
    })
}

const EMAIL_RX = /^[^\s@]+@[^\s@]+\.[^\s@]+$/

function emailValid() {
  const val = $('#email').val()
  return val != '' && EMAIL_RX.test(val)
}

function emailError() {
  $('#email_error').removeClass('hidden')
}

function emailErrorClear() {
  $('#email_error').addClass('hidden')
}

window.goTo = goTo

if (/complete|interactive|loaded/.test(document.readyState)) {
  onContentLoad()
} else {
  window.addEventListener('DOMContentLoaded', onContentLoad)
}
